﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace snoop.Models
{
    public class Jugador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string FechaNac { get; set; }
        public string Cotizacion { get; set; }
    }
}