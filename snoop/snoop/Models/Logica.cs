﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace snoop.Models
{
    public class Logica
    {

        public List<Jugador> ListaJugadores()
        {
            var LPersona = new List<Jugador>();
            using (StreamReader reader = new StreamReader("C:/Temp/Personas.txt"))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!String.IsNullOrWhiteSpace(line))
                    {
                        string[] values = line.Split(';');
                        if (values.Length > 0)
                        {
                            var oJugador = new Jugador();
                            oJugador.Id = Convert.ToInt32(values[0]);
                            oJugador.Nombre = values[1];
                            oJugador.FechaNac = values[2];
                            oJugador.Cotizacion = values[3];
                            LPersona.Add(oJugador);
                        }
                    }
                }
                return LPersona;

            }
        }

    }
}