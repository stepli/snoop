﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using snoop.Models;
namespace snoop.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult JugadoresList(int? Page, int? PageSize)
        {
            int PageSizer = 5;
            int PageIndex = 1;
            PageSizer = PageSize.HasValue ? Convert.ToInt32(PageSize) : 5;
            PageIndex = Page.HasValue ? Convert.ToInt32(Page) : 1;

            ViewBag.psize = PageSizer;
            ViewBag.PageSize = new List<SelectListItem>()
            {
             new SelectListItem() { Value="5", Text= "5" },
             new SelectListItem() { Value="10", Text= "10" },
             new SelectListItem() { Value="15", Text= "15" },
             new SelectListItem() { Value="20", Text= "20" },
             new SelectListItem() { Value="25", Text= "25" },
            };

            IPagedList<Jugador> PlPer = null;
            var Logica = new Logica();
            var LJugadores = new List<Jugador>();
            LJugadores = Logica.ListaJugadores();
            PlPer = LJugadores.ToPagedList(PageIndex, PageSizer);
            return View(PlPer);
        }
    }
}